package tests;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import logica.Grafo;
import logica.Manzana;
import logica.Solver;

public class SolverTest {
	
	private Grafo crearManzanas() {
		
		Manzana a = new Manzana(1);
		Manzana b = new Manzana(2);
		Manzana c = new Manzana(3);
		Manzana d = new Manzana(4);
		Manzana e = new Manzana(5);
		Manzana f = new Manzana(6);
		Manzana h = new Manzana(7);
		Manzana i = new Manzana(8);
		
		
		Grafo g = new Grafo();
		g.agregarVertice(a);
		g.agregarVertice(b);
		g.agregarVertice(c);
		g.agregarVertice(d);
		g.agregarVertice(e);
		g.agregarVertice(f);
		g.agregarVertice(h);
		g.agregarVertice(i);
		
		g.agregarRelacionEntreVertices(a, b);
		g.agregarRelacionEntreVertices(a, e);
		g.agregarRelacionEntreVertices(b, c);
		g.agregarRelacionEntreVertices(b, f);
		g.agregarRelacionEntreVertices(c, d);
		g.agregarRelacionEntreVertices(c, h);
		g.agregarRelacionEntreVertices(d, i);
		g.agregarRelacionEntreVertices(e, f);
		g.agregarRelacionEntreVertices(f, h);
		g.agregarRelacionEntreVertices(h, i);
		
		return g;
	}
	
	@Test (expected = RuntimeException.class)
	public void testManzanasSinAsignar() {
		Grafo g = crearManzanas();
		ArrayList<String> sencistas = new ArrayList<String>();
		sencistas.add("Carlos");
		sencistas.add("Emma");

		Solver s = new Solver(g,sencistas);
		s.solverManzanas();
		
	}

	
	@Test 
	public void testSolver() {
	
		ArrayList<String> sencistas = new ArrayList<String>();
		sencistas.add("Carlos");
		sencistas.add("Emma");
		sencistas.add("Pepe");
		sencistas.add("Matias");
		
		Grafo g = crearManzanas();
		Solver s = new Solver(g,sencistas);
		s.solverManzanas();

		assertEquals(3, s.getSolver().get("Carlos").size());
		assertEquals(3, s.getSolver().get("Emma").size());
		assertEquals(2, s.getSolver().get("Pepe").size());
		assertEquals(0, s.getSolver().get("Matias").size());
	}
	

}
