package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import logica.Grafo;
import logica.Manzana;

public class GrafoTest {
	
	@Test
	public void cargarVerticesTest() {

		Grafo g = new Grafo();
		Manzana a = new Manzana(1);
		Manzana b = new Manzana(2);
		
		g.agregarVertice(a);
		g.agregarVertice(b);
		
		g.agregarRelacionEntreVertices(a, b);
		
		assertEquals(2, g.getListaDeVecinos().size());		
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void cargarVerticeRepetidoTest(){
		
		Grafo g = new Grafo();
		Manzana a = new Manzana(1);
		Manzana b = new Manzana(1);
		
		g.agregarVertice(a);
		g.agregarVertice(b);		
	}
	

	@Test
	public void agregarRelacionTest(){
		
		Grafo g = new Grafo();
		
		Manzana a = new Manzana(1);
		Manzana b = new Manzana(2);
		
		g.agregarVertice(a);
		g.agregarVertice(b);
		
		assertTrue(g.agregarRelacionEntreVertices(a, b));
		assertEquals(1, g.getCantidadDeRelaciones());		
	}
	
	
	@Test
	public void chequearListaDeVecinosTest(){
		
		Grafo g = new Grafo();
		
		Manzana a = new Manzana(1);
		Manzana b = new Manzana(2);
		
		g.agregarVertice(a);
		g.agregarVertice(b); 
		
		g.agregarRelacionEntreVertices(a, b);
		
		assertEquals(2, g.getListaDeVecinos().size());		
	}
	
}


