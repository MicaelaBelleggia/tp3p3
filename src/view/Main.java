package view;

import javax.swing.JFrame;
import controller.CensoController;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.ImageIcon;
import java.awt.Toolkit;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Main {

	private JFrame frmCenso;
	private CensoController censo;
	private JTextField inputCensistas;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frmCenso.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public Main() {
		censo = new CensoController();
		initialize();
	}

	private void initialize() {
		frmCenso = new JFrame();
		frmCenso.setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\agust\\OneDrive\\Escritorio\\tp3p3\\src\\view\\img\\arg.png"));
		frmCenso.setTitle("CENSO 2022");
		frmCenso.getContentPane().setBackground(Color.WHITE);
		frmCenso.setBounds(100, 100, 318, 209);
		frmCenso.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCenso.getContentPane().setLayout(null);
		
		JLabel labelAgregarCencista = new JLabel("Agregar censista");
		labelAgregarCencista.setFont(new Font("Georgia", Font.PLAIN, 10));
		labelAgregarCencista.setBounds(10, 51, 86, 14);
		frmCenso.getContentPane().add(labelAgregarCencista);
		
		JButton consultaCencistasButton = new JButton("Tus cencistas");
		consultaCencistasButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFrame listaCencistas = Censistas.cencistasRegistrados(censo.getCencistas());
				listaCencistas.setVisible(true);
			}
		});
		consultaCencistasButton.setFont(new Font("Georgia", Font.PLAIN, 10));
		consultaCencistasButton.setBounds(179, 47, 113, 23);
		frmCenso.getContentPane().add(consultaCencistasButton);
		
		JLabel titulo = new JLabel("Censo 2022");
		titulo.setFont(new Font("Georgia", Font.PLAIN, 18));
		titulo.setHorizontalAlignment(SwingConstants.CENTER);
		titulo.setBounds(10, 11, 282, 25);
		frmCenso.getContentPane().add(titulo);
		
		JLabel iconBackground = new JLabel("");
		iconBackground.setIcon(new ImageIcon("C:\\Users\\agust\\OneDrive\\Escritorio\\tp3p3\\src\\view\\img\\arg.png"));
		iconBackground.setBounds(-19, 64, 130, 128);
		frmCenso.getContentPane().add(iconBackground);
		
		inputCensistas = new JTextField();
		inputCensistas.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyChar() == java.awt.event.KeyEvent.VK_ENTER) {
					if(inputCensistas.equals("")) {
						return;
					}
					String nombreCensista = inputCensistas.getText();
					censo.agregarCencista(nombreCensista);
					inputCensistas.setText("");
					}
			}
		});
		inputCensistas.setBounds(102, 48, 67, 20);
		frmCenso.getContentPane().add(inputCensistas);
		inputCensistas.setColumns(10);
		
		JButton exeCensoButton = new JButton("EJECUTAR CENSO");
		exeCensoButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					censo.cargarGrafo(CensoData.datos());
					HashMap<String, HashSet<Integer>> resultados = censo.ejecutarCenso();
					JFrame resultadosVista = Censistas.manzanasAsignadasCencistas(resultados);
					resultadosVista.setVisible(true);
				}catch(Exception err) {
					WindowMessage error = new WindowMessage("Error", err.getMessage());
					error.setVisible(true);
				}
			}
		});
		exeCensoButton.setBounds(130, 102, 149, 31);
		frmCenso.getContentPane().add(exeCensoButton);
	}
}
