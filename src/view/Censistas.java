package view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import javax.swing.SwingConstants;

public class Censistas {
	
	private static JFrame cencistasWindow;

	public static JFrame cencistasRegistrados(ArrayList<String> cencistas) {
		cencistasWindow = new JFrame();
		cencistasWindow.setTitle("Lista de cencistas");
		cencistasWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		cencistasWindow.setBounds(100, 100, 450, 300);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		cencistasWindow.setContentPane(contentPane);
		contentPane.setLayout(null);
		
		if(cencistas.isEmpty()) {
			contentPane.setLayout(null);
			JLabel msj = new JLabel("No hay censistas.");
			msj.setHorizontalAlignment(SwingConstants.CENTER);
			msj.setBounds(10, 121, 414, 14);
			contentPane.add(msj);
		}else {
			contentPane.setLayout(new GridLayout(cencistas.size(), 0, 0, 0));
			cencistas.stream().forEach(c -> {
				JLabel aux = new JLabel(c);
				aux.setHorizontalAlignment(SwingConstants.CENTER);
				contentPane.add(aux);
			});
		}
		return cencistasWindow;
	}
	
	public static JFrame manzanasAsignadasCencistas(HashMap<String, HashSet<Integer>> manzanasAsignadas) {
		cencistasWindow = new JFrame();
		cencistasWindow.setTitle("Asignacion de manzanas");
		cencistasWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		cencistasWindow.setBounds(100, 100, 450, 300);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		cencistasWindow.setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(manzanasAsignadas.size(), 0, 0, 0));
		for(String cencista : manzanasAsignadas.keySet()) {
			JLabel msj = new JLabel("El censista " + cencista + " tiene asignadas las siguientes manzanas: " + manzanasAsignadas.get(cencista).toString());
			msj.setHorizontalAlignment(SwingConstants.CENTER);
			contentPane.add(msj);
		}
		return cencistasWindow;
	}
}
