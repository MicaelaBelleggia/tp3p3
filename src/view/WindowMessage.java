package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import javax.swing.SwingConstants;

public class WindowMessage extends JFrame {

	private String titulo;
	private String mensaje;

	public WindowMessage(String t, String m) {
		titulo = t;
		mensaje = m;
		initialize();
	}

	private void initialize() {
		
		setTitle(titulo);
		setBounds(100, 100, 452, 122);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel(mensaje);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(10, 11, 416, 61);
		add(lblNewLabel);
	}
}
