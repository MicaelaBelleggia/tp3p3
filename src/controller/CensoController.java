package controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import logica.Grafo;
import logica.Manzana;
import logica.Solver;

public class CensoController {

	private Grafo _g;
	private Solver _censo;
	private ArrayList<String> _cencistas;

	public CensoController() {
		_g = new Grafo();
		_cencistas = new ArrayList<>();
	}

	public HashMap<String, HashSet<Integer>> ejecutarCenso() {
		_censo = new Solver(_g, _cencistas);

		return _censo.solverManzanas();
	}
	
	public void cargarGrafo(Grafo g) {
		_g = g;
	}

	public void agregarCencista(String cencista) {
		if (!_cencistas.contains(cencista))
			_cencistas.add(cencista);
	}

	public ArrayList<String> getCencistas() {
		return (ArrayList<String>) _cencistas.clone();
	}
}
