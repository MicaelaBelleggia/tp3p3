package logica;

import java.util.HashSet;

public class Manzana {
	private Integer _numero;
	private HashSet<Manzana> _vecinos;
	private boolean _asignada;
	
	public Manzana (Integer numero) {
		this._numero = numero;
		_asignada = false;
		_vecinos = new HashSet<Manzana>();
	}
	
	public Manzana(Integer numero, HashSet<Manzana>vecinos) {
		this._numero = numero;
		this._vecinos = vecinos;
		this._asignada = false;
	}
	
	public void agregarVecinos(Manzana m) {
			_vecinos.add(m);
	}
	
	public Integer getNumero() {
		return _numero;
	}
	public HashSet<Manzana> getVecinos() {
		return _vecinos;
	}
	public boolean isAsignada() {
		return _asignada;
	}
	public void setNumero(Integer numero) {
		this._numero = numero;
	}
	public void setVecinos(HashSet<Manzana> vecinos) {
		this._vecinos = vecinos;
	}
	public void setAsignada(boolean asignada) {
		this._asignada = asignada;
	}

}
