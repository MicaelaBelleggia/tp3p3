package logica;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Grafo {

	private ArrayList<Arista> _listaDeAristas;
	private ArrayList<Manzana> _manzanas;
	private HashMap<Manzana, HashSet<Manzana>> _listaDeVecinos;

	public Grafo() {
		this._listaDeAristas = new ArrayList<Arista>();
		this._manzanas = new ArrayList<Manzana>();
		this._listaDeVecinos = new HashMap<Manzana, HashSet<Manzana>>();
	}

	public boolean agregarVertice(Manzana vertice) {

		boolean agregado = false;

		chequearVerticeExistenteEnGrafo(vertice);

		_manzanas.add(vertice);
		agregado = true;
		return agregado;
	}

	private void chequearVerticeExistenteEnGrafo(Manzana vertice) {
		for (Manzana manzana : _manzanas) {
			if (manzana.getNumero() == vertice.getNumero())
				throw new IllegalArgumentException("�Ese vertice ya existe!");
		}
	}

	public boolean agregarRelacionEntreVertices(Manzana vertice1, Manzana vertice2) {

		boolean aristaAgregada = false;

		if (!_manzanas.contains(vertice1) || !_manzanas.contains(vertice2))
			return aristaAgregada;

		chequearRelacionesCirculares(vertice1, vertice2);

		Arista aristaNueva = new Arista(vertice1, vertice2);
		boolean isAristaExistente = false;

		for (Arista a : _listaDeAristas) {
			if (a.equals(aristaNueva))
				isAristaExistente = true;
		}

		if (!isAristaExistente) {
			_listaDeAristas.add(aristaNueva);
			agregarAListaDeVecinos(vertice1, vertice2);
			agregarAListaDeVecinos(vertice2, vertice1);
			aristaAgregada = true;
		}
		return aristaAgregada;
	}

	private void chequearRelacionesCirculares(Manzana vertice1, Manzana vertice2) {
		if (vertice1.getNumero() == vertice2.getNumero()) {
			throw new IllegalArgumentException("Ese n�mero de manzana ya est� repetido.");
		}
	}

	private void agregarAListaDeVecinos(Manzana vertice1, Manzana vertice2) {

		if (!_listaDeVecinos.containsKey(vertice1)) {
			_listaDeVecinos.put(vertice1, new HashSet<Manzana>());
			_listaDeVecinos.get(vertice1).add(vertice2);

		} else {
			_listaDeVecinos.get(vertice1).add(vertice2);
		}
		vertice1.agregarVecinos(vertice2);
		vertice2.agregarVecinos(vertice1);
	}

	public boolean getExistenciaDeVertice(Manzana m) {
		return _listaDeVecinos.containsKey(m);
	}

	public ArrayList<Arista> getRelacionesEntreVertices() {
		return _listaDeAristas;
	}

	public ArrayList<Manzana> getListaDeVertices() {
		return _manzanas;
	}

	public int getCantidadManzanas() {
		return _manzanas.size();
	}

	public HashMap<Manzana, HashSet<Manzana>> getListaDeVecinos() {
		return _listaDeVecinos;
	}

	public Manzana getVertice(int i) {
		return _manzanas.get(i);
	}

	public int indiceVertice(Integer i) {
		return _manzanas.indexOf(i);
	}

	public Set<Manzana> getVecinosDeUnVertice(Manzana m) {

		Set<Manzana> ret = new HashSet<Manzana>();

		if (getExistenciaDeVertice(m))
			ret = _listaDeVecinos.get(m);

		return ret;
	}

	public int getCantidadDeRelaciones() {
		return _listaDeAristas.size();
	}

	public String toString() {
		return "Manzanas: " + _manzanas.toString() + ". Relaciones: " + this._listaDeVecinos.toString();
	}

}
