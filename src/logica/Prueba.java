package logica;

import java.util.ArrayList;
import java.util.HashSet;

public class Prueba {
	
	
	public static HashSet<Integer> asignarManzanas(HashSet<Integer> planilla, Manzana manzanaInicial) {

		System.out.println(manzanaInicial.getNumero());
		for(Manzana vecina : manzanaInicial.getVecinos()) {
			System.out.println("Vecino en lista de vecinos:" + vecina.getNumero());
		}
		
		planilla.add(manzanaInicial.getNumero());
		manzanaInicial.setAsignada(true);

		for (Integer i : planilla) { 
			System.out.println("Numero en planilla: " + i);
		}
		
		
		Manzana vecinoVacio = null;
		boolean estadoVecinos= true; 
		
		
		for (Manzana vecino : manzanaInicial.getVecinos()) {
			
			System.out.println("VecinoActual: " + vecino.getNumero() + " Estado: " + vecino.isAsignada());
			
			estadoVecinos= estadoVecinos && vecino.isAsignada();//si hay al menos un vecino sin asignar

			System.out.println("EstadoVecinos:" + estadoVecinos);

			if (vecino.isAsignada() == false) 
				vecinoVacio = vecino;
		}
		
		
		//Caso Base
		
		if(vecinoVacio == null || estadoVecinos == true || planilla.size() == 3) 
			return planilla;   
		
		//Recursión 
		return asignarManzanas(planilla, vecinoVacio);
	}

	public static void main(String[] args) {
	
		Manzana a = new Manzana(1);
		Manzana b = new Manzana(2);
		Manzana c = new Manzana(3);
		Manzana d = new Manzana(4);
		Manzana e = new Manzana(5);
		Manzana f = new Manzana(6);
		Manzana h = new Manzana(7);
		Manzana i = new Manzana(8);
		
		
		Grafo g = new Grafo();
		g.agregarVertice(a);
		g.agregarVertice(b);
		g.agregarVertice(c);
		g.agregarVertice(d);
		g.agregarVertice(e);
		g.agregarVertice(f);
		g.agregarVertice(h);
		g.agregarVertice(i);
		
		g.agregarRelacionEntreVertices(a, b);
		g.agregarRelacionEntreVertices(a, e);
		g.agregarRelacionEntreVertices(b, c);
		g.agregarRelacionEntreVertices(b, f);
		g.agregarRelacionEntreVertices(c, d);
		g.agregarRelacionEntreVertices(c, h);
		g.agregarRelacionEntreVertices(d, i);
		g.agregarRelacionEntreVertices(e, f);
		g.agregarRelacionEntreVertices(f, h);
		g.agregarRelacionEntreVertices(h, i);
		
			
		ArrayList<String> sencistas = new ArrayList<String>();
		sencistas.add("Carlos");
		sencistas.add("Emma");
//		sencistas.add("Pepe");
//		sencistas.add("Matias");
//		sencistas.add("Pedro");
		
		Solver s = new Solver(g,sencistas);
		s.solverManzanas();
		System.out.println(s.getSolver());
		
	//	HashSet<Integer> manzanasContiguas = g.getListaDeVecinos().get(1);
	//	HashSet<Integer> manzanasContiguas = _g.getListaDeVecinos().get(i)
//		System.out.println(manzanasContiguas); 
//
//		System.out.println(g.getListaDeVecinos().get(1)); 
		System.out.println(s.getSolver());
		
		
		

//		
//		HashSet<Integer> planillaA = new HashSet<Integer>();
//		HashSet<Integer> planillaB = new HashSet<Integer>();
//		HashSet<Integer> planillaC = new HashSet<Integer>();
//		
//		Manzana manzanaInicial = g.getVertice(0);
//		Manzana manzanaInicial2 = g.getVertice(1);
//		Manzana manzanaInicial3 = g.getVertice(3);
//
//		HashSet<Integer> planilla1 = asignarManzanas(planillaA, manzanaInicial);
//		System.out.println(planilla1);
//		HashSet<Integer> planilla2 = asignarManzanas(planillaB, manzanaInicial2);
//		System.out.println(planilla2);
//		HashSet<Integer> planilla3 = asignarManzanas(planillaC, manzanaInicial3);
//		System.out.println(planilla3);
//	
//		System.out.println("Planilla 1:" + planilla1);
//		System.out.println("Planilla 2:" + planilla2);
//		System.out.println("Planilla 3:" + planilla3);
	}

}
