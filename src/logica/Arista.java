package logica;

import java.util.HashSet;
import java.util.Set;

public class Arista {
	private Manzana _vertice1;
	private Manzana _vertice2;
	
	public Arista (Manzana v1, Manzana v2) {
		_vertice1 = v1;
		_vertice2 = v2;
	}
	
	public int getVertice1() {
		return _vertice1.getNumero();
	}

	public int getVertice2() {
		return _vertice2.getNumero();
	}
	
	private boolean isVerticeEnRelacion(Manzana vertice) {
		Set<Manzana> temp = new HashSet<Manzana>();
		temp.add(this._vertice1);
		temp.add(this._vertice2);
		return temp.contains(vertice);
	}
	
}
