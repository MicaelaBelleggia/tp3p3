package logica;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;

public class Solver {
	
	private HashMap<String, HashSet<Integer>> _asignacion; 
	private Grafo _g;
	
	public Solver(Grafo g, ArrayList<String> listaCensistas) {
		_asignacion = new LinkedHashMap<String,HashSet<Integer>>();
		_g = g;
		for (int i = 0; i< listaCensistas.size(); i++) {
			_asignacion.put(listaCensistas.get(i), new HashSet<Integer>());
		}
	}
	
	public HashMap<String, HashSet<Integer>> solverManzanas( ) {
		for(String censista : _asignacion.keySet()) {
			if(!isTodasManzanasAsignadas()) {
				HashSet<Integer> planillaCensista = _asignacion.get(censista);
				Manzana manzanaSinAsignar = _g.getVertice(getVerticeInicial());
				HashSet<Integer> nuevaPlanilla = asignarManzanas(planillaCensista, manzanaSinAsignar); //Devuelve una planilla con 3 manzanas maximo 
			}	
		}
		if(!isTodasManzanasAsignadas()) throw new RuntimeException("Quedaron manzanas sin asignar, debe contratar mas censistas.");
		return _asignacion;
	}
	
	private Integer getVerticeInicial() {
		Integer verticeElegido = 0;
				for (int i=0; i<_g.getCantidadManzanas();i++) {
					if (!_g.getVertice(i).isAsignada()) {
						verticeElegido= i;
						break;
					}
				}
					
				return verticeElegido;
			}
	
	private HashSet<Integer> asignarManzanas(HashSet<Integer> planilla, Manzana manzanaInicial) {

		planilla.add(manzanaInicial.getNumero());
		manzanaInicial.setAsignada(true);
		
		Manzana vecinoVacio = null;
		boolean estadoVecinos= true; 
		
		for (Manzana vecino : manzanaInicial.getVecinos()) {
			
			estadoVecinos= estadoVecinos && vecino.isAsignada();//si hay al menos un vecino sin asignar

			if (vecino.isAsignada() == false) 
				vecinoVacio = vecino;
		}
		//Caso Base
		if(vecinoVacio == null || estadoVecinos == true || planilla.size() == 3) 
			return planilla;   	
		//Recursión 
		return asignarManzanas(planilla, vecinoVacio);	
		}

	private boolean isTodasManzanasAsignadas() {
		ArrayList<Manzana> manzanas = _g.getListaDeVertices();
		boolean todasManzanasAsignadas = true;
		for(Manzana m : manzanas) 
			todasManzanasAsignadas = todasManzanasAsignadas && m.isAsignada();
		return todasManzanasAsignadas;
	}
	
	public HashMap<String, HashSet<Integer>> getSolver() {
		return _asignacion;
	}
}
